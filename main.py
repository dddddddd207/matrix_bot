import json
import sys
import re
import feedparser
import threading
import time
import asyncio
import simplematrixbotlib as botlib

# ==============================================================================
# to run the bot:
# python main.py user_creds.creds
# where user.creds is a json file with the following format:
# {
#     "homeserver": HOMESERVER,
#     "username": USERNAME,
#     "password": PASSWORD,
# }
# ...where the all caps bits are the values enclosed in quotes

# ==============================================================================
# functions
# if you want a helper function put it here. you should make these functions expect a list of strings
# which probably means you want to just pass in match.args() to this function
def nitter(message):
    message = message[0]
    return message.replace("twitter.com", "nitter.net")    

# ==============================================================================
# initialization
# don't mess with this
creds = None
with open(sys.argv[1]) as creds_file:
    creds = json.load(creds_file)
bot_creds = botlib.Creds(creds['homeserver'], creds['username'], creds['password'])
BOT = botlib.Bot(bot_creds)
config = botlib.Config()
config.join_on_invite = True

# ==============================================================================
# constants
# add big messages and whatever here
PREFIX = '!'
HELP = """Usage: ![command], where [command] is:
* help: prints this message
* echo: prints everything after the command
* nitter: give it a twitter link and it'll print the equivalent nitter.net link
* ayy: lmao
"""
BOT_TESTING_ROOM_ID = creds["bot_testing_room_id"]

# ==============================================================================
# while loop for the bot to auto post stuff
# put functions you want to call above the loop, then call them in the loop
# each function should handle sending the message itself
class Forever:
    isRunning = False
    def __init__(self):
        # if you need initial setup, put it here
        self.zeroair_feed = feedparser.parse("https://zeroair.org/feed")
        self.zeroair_lastDate = self.zeroair_feed.entries[0].published_parsed

    # add whatever varibles you need here
    zeroair_feed = None
    zeroair_lastDate = None
    flashlightRoomId = creds["flashlight_room_id"]

    # add your function here
    async def zeroair(self):
        curDate = self.zeroair_feed.entries[0].published_parsed
        curLink = self.zeroair_feed.entries[0].links[0].href
        if curDate > self.zeroair_lastDate:
            await BOT.api.send_text_message(flashlightRoomId, curLink)

    async def refresh(self):
        if not self.isRunning:
            self.isRunning = True
            while True:
                await asyncio.sleep(10)
                
                # add your function calls here
                await self.zeroair()

# don't touch this part
forever_instance = Forever()
@BOT.listener.on_startup
async def bleh(_):
    asyncio.create_task(forever_instance.refresh())

# ==============================================================================
# command handler
# add your commands here
# to add a command, add these two lines:
# 
# elif match.command(COMMAND):
#     await BOT.api.send_text_message(room.room_id, RESULT)
#
# ...where COMMAND is the command as a string, of course, and RESULT is what the bot sends back, also as a string
# add these lines before the ending else: clause
@BOT.listener.on_message_event
async def handler(room, message):
    match = botlib.MessageMatch(room, message, BOT, PREFIX)
    if match.is_not_from_this_bot() and match.prefix():
        if match.command("echo"):
            await BOT.api.send_text_message(room.room_id, " ".join(arg for arg in match.args()))
        elif match.command("ayy"):
            await BOT.api.send_text_message(room.room_id, "lmao")
        elif match.command("ayy_lmao"):
            await BOT.api.send_image_message(room.room_id, "ayy.png")
        elif match.command("nitter"):
            await BOT.api.send_text_message(room.room_id, nitter(match.args()))
        elif match.command("help"):
            await BOT.api.send_text_message(room.room_id, HELP)
        elif match.command("???"):
            question_marks = "??????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????"
            await BOT.api.send_text_message(room.room_id, question_marks)

        # add your commands above this line
        # keep this command commented when deployed; uncomment it for development purposes
        # when you comment it out make sure the leading """ is at the appropriate indentation
        # because python doesn't have multiline comments so you have to use a multiline string not assigned to a varible
        # instead, and one that's incorrectly indented yould break this if-elif-else chain
            """
        elif match.command("debug"):
            await BOT.api.send_text_message(room.room_id, room.room_id)
            """
        else:
            await BOT.api.send_text_message(room.room_id, "I see you're tying to enter a command. Would you like some help?\n" + HELP)

# ==============================================================================
# this actually runs the thing. don't touch
# not that there's a lot to touch here anyway
BOT.run()

# ==============================================================================
# if you need more help, here's the docs:
# https://simple-matrix-bot-lib.readthedocs.io/en/latest/index.html
#
# and the repo:
# https://github.com/i10b/simplematrixbotlib
